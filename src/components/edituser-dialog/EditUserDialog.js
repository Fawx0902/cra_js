import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { useState } from 'react';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// @mui
import {
  Alert,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Stack,
  Step,
  Stepper,
  StepLabel,
  Typography,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
// components
import FormProvider, { RHFTextField, RHFSelect } from '../hook-form';
import { useSnackbar } from '../snackbar';
// API
import { updateUser } from '../../pages/auth/API/user';
// ----------------------------------------------------------------------
EditUserDialog.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.node,
    onClose: PropTypes.func,
    rolesNames: PropTypes.array,
    row: PropTypes.object,
  };

export default function EditUserDialog({ title, open, onClose, rolesNames, row, ...other }) {

    const { id, name, email, role } = row;

    const steps = ['Enter User Details', 'Review Information'];
    const [activeStep, setActiveStep] = useState(0);
    const [doubleCheck, setDoubleCheck] = useState(false);
    // doubleCheck is used to prevent onSubmit automatically triggering when going from step 1 - 2 and vice versa without tapping the Edit button

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        clearErrors();
    };

    const handleBack = () => {
        setDoubleCheck(false);
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
        setDoubleCheck(false);
        clearErrors();
    };

    const { enqueueSnackbar } = useSnackbar();

    const UserSchema = Yup.object().shape({
        name: Yup.string().required('Name is required!'),
        email: Yup.string().required('Email is required!').email('Email must be a valid email address..'),
        role: Yup.string().required('Role is required!'),
    });

    const defaultValues = {
        name,
        email,
        role,
    }

    const methods = useForm({
        resolver: yupResolver(UserSchema),
        defaultValues,
    });

    const {
      reset,
      setError,
      clearErrors,
      handleSubmit,
      watch,
      formState: {errors},
    } = methods;

    const userValues = watch();

    const canProceedToNextStep = async () => {
      if (!Object.keys(errors).length) {
          console.log("No Error!");
      } else {
          console.log("Error Detected!");
          handleBack();
      }
  }
    
    const onSubmit = async (data) => {
        try {
            canProceedToNextStep();
            if (!doubleCheck){
                setDoubleCheck(true);
                return;
            }
            setDoubleCheck(false);
            const response = await updateUser(id, data.name, data.email, data.role);
            if (response.error) {
                handleBack();
                setError('afterSubmit', {
                  message: response.error,
                });
                return;
              }

            enqueueSnackbar('Update user success!' , {variant:"success"});
            onClose();
            reset();

        } catch (error) {
            enqueueSnackbar('Something went wrong..', {variant:"error"});
            setError('afterSubmit', {
                ...error,
                message: error.message,
              });
            console.log(error);
        }
    };  

  return (
      <Dialog fullWidth maxWidth="xs" open={open} onClose={() => { onClose(); reset(); handleReset(); }}>
        <DialogTitle sx={{ pb: 2 }}>{title}</DialogTitle>
        <IconButton
          aria-label="close"
          onClick={() => { onClose(); reset(); handleReset(); }}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <Stepper activeStep={activeStep}>
            {steps.map((label, index) => {
                const stepProps = {};
                const labelProps = {};
                return (
                    <Step key = {label} {...stepProps} sx={{ px: 3, pb: 3 }}>
                        <StepLabel {...labelProps}>{label}</StepLabel>
                    </Step>
                );
            })}
        </Stepper>
            <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
                <DialogContent dividers>
                {activeStep === steps.length - 1 ? (
                <Stack spacing={3}>
                    <Typography align='center' sx={{ mb: 3, mt: 1, mx: 1, }}>Review your information!</Typography> 

                    <Typography align='center'>Name: {userValues.name}</Typography> 

                    <Typography align='center'>Email: {userValues.email}</Typography> 

                    <Typography align='center'>Role: {userValues.role}</Typography>  
                </Stack>
                        
                ) : ( 
                  <Stack spacing={3}>
                    {!!errors.afterSubmit && !!errors.afterSubmit.message.length && <Alert severity="error">
                    <ul>
                    {errors.afterSubmit.message.map((error, index) => (
                        <li key={index}>{error}</li>
                    ))}
                    </ul>
                    </Alert>}

                        <RHFTextField
                            name="name"
                            label="Name"
                            sx={{ mt: 1 }}
                        />
                        <RHFTextField
                            name="email"
                            label="Email Address"
                        />

                        <RHFSelect native name="role" label="Status" placeholder="Status">
                            <option value = "" />
                            {rolesNames ? (
                            rolesNames
                                .filter((option) => option.isActive === 1 && option.deleted_at === null)
                                .map((option) => (
                                <option key={option.id} value={option.role}>
                                    {option.role}
                                </option>
                                ))
                            ) : (
                                <option value = 'null'>
                                Loading...
                                </option>
                            )}
                        </RHFSelect>
                    </Stack>
                 )}
                </DialogContent>  
                <DialogActions>
                <Button color="inherit" disabled={activeStep === 0} onClick={handleBack} type="button">
                    Back
                </Button>
                {activeStep === steps.length - 1 ? (
                    <Button variant="contained" color="success" type="submit" onClick={canProceedToNextStep}>
                        Edit User
                    </Button>
                ) : (
                    <Button variant="contained" onClick={() => {handleNext();}}>
                        Next
                    </Button>
                )}
            </DialogActions>      
          </FormProvider>    
        </Dialog>
  );
}
