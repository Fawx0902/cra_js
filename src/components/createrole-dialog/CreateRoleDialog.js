import PropTypes from 'prop-types';
import { useState } from 'react';
import * as Yup from 'yup';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// @mui
import {
  Alert,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControlLabel,
  IconButton,
  Stack,
  Switch,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
// components
import FormProvider, { RHFTextField, RHFSelect } from '../hook-form';
import { useSnackbar } from '../snackbar';
// API
import { createRole } from '../../pages/auth/API/role';
// ----------------------------------------------------------------------
CreateRoleDialog.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.node,
    onClose: PropTypes.func,
    user: PropTypes.object,
    socket: PropTypes.object,
  };

export default function CreateRoleDialog({ title, open, onClose, user, socket, ...other }) {

    const { enqueueSnackbar } = useSnackbar();

    const [permission, setPermission] = useState({
        employeemodule: false,
        usermanagement: false,
        rolemodule: false,
    });

    const handlePermissionChange = (event) => {
        setPermission({
            ...permission,
            [event.target.name]: event.target.checked,
        });
    };

    const RoleSchema = Yup.object().shape({
        role: Yup.string().required('Role is required.'),
        description: Yup.string().required('Description is required.'),
        isActive: Yup.string().required('Status is required.'),
    });

    const defaultValues = {
        role: '',
        description: '',
        isActive: '',
    }

    function resetPermission() {
        setPermission({
            employeemodule: false,
            usermanagement: false,
            rolemodule: false,
        });
    }

    const methods = useForm({
        resolver: yupResolver(RoleSchema),
        defaultValues,
    });

    const {
      reset,
      setError,
      handleSubmit,
      formState: {errors},
    } = methods;
    
    const onSubmit = async (data) => {
        try {
            const formattedPermissions = Object.keys(permission)
            .filter((key) => permission[key])
            .map((key) => `/${key}`);
            const response = await createRole(data.role, data.description, formattedPermissions, data.isActive);
            if (response.error) {
                setError('afterSubmit', {
                  message: response.error,
                });
                return;
            }
            socket.emit("Add!", {message: `${user?.name} added a new role ${data.role}!`});
            enqueueSnackbar('Role creation success!' , {variant:"success"});
            reset();
            resetPermission();
            onClose();
        } catch (error) {
            enqueueSnackbar('Something went wrong..', {variant:"error"});
            console.log(error);
        }
    };  

  return (
      <Dialog fullWidth maxWidth="xs" open={open} onClose={() => { onClose(); reset(); resetPermission(); }}>
        <DialogTitle sx={{ pb: 2 }}>{title}</DialogTitle>
        <IconButton
          aria-label="close"
          onClick={() => { onClose(); reset(); resetPermission(); }}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
            <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
                <DialogContent dividers>
                    <Stack spacing={3}>
                    {!!errors.afterSubmit && <Alert severity="error">{errors.afterSubmit.message}</Alert>}
                        <RHFTextField
                            name="role"
                            label="Role"
                            sx={{ mt: 1 }}
                        />
                        <RHFTextField
                            name="description"
                            label="Description"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={permission.employeemodule} onChange={handlePermissionChange} name="employeemodule" />
                            }
                            label="Employee Module"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={permission.usermanagement} onChange={handlePermissionChange} name="usermanagement" />
                            }
                            label="User Management"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={permission.rolemodule} onChange={handlePermissionChange} name="rolemodule" />
                            }
                            label="Role Module"
                        />
                        <RHFSelect native name="isActive" label="Status" placeholder="Status">
                            <option value = "" />
                            <option value = "1">Active</option>
                            <option value = "0">Inactive</option>
                        </RHFSelect>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button fullWidth variant="contained" color="success" type="submit">
                        Create Role
                    </Button>
                </DialogActions>
            </FormProvider>
      </Dialog>
  );
}
