import PropTypes from 'prop-types';
import { useState } from 'react';
import * as Yup from 'yup';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// @mui
import {
  Alert,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControlLabel,
  IconButton,
  Stack,
  Switch,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
// components
import FormProvider, { RHFTextField } from '../hook-form';
import { useSnackbar } from '../snackbar';
// API
import { updateRole } from '../../pages/auth/API/role';
// ----------------------------------------------------------------------
EditRoleDialog.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.node,
    onClose: PropTypes.func,
    row: PropTypes.object,
    user: PropTypes.object,
    socket: PropTypes.object,
  };

export default function EditRoleDialog({ title, open, onClose, row, user, socket, ...other }) {

    const { id, role, description, permission} = row;

    const { enqueueSnackbar } = useSnackbar();

    const [prefillPermission, setPermission] = useState({
        employeemodule: permission.includes('/employeemodule'),
        usermanagement: permission.includes('/usermanagement'),
        rolemodule: permission.includes('/rolemodule'),
    });

    const handlePermissionChange = (event) => {
        setPermission((prevPermissions) => ({
            ...prevPermissions,
            [event.target.name]: event.target.checked,
        }));
    };

    const RoleSchema = Yup.object().shape({
        role: Yup.string().required('Role is required.'),
        description: Yup.string().required('Description is required.'),
    });

    const defaultValues = {
        role,
        description,
    }

    function resetPermission() {
        setPermission({
            employeemodule: permission.includes('/employeemodule'),
            usermanagement: permission.includes('/usermanagement'),
            rolemodule: permission.includes('/rolemodule'),
        });
    }

    const methods = useForm({
        resolver: yupResolver(RoleSchema),
        defaultValues,
    });

    const {
        reset,
        setError,
        handleSubmit,
        formState: {errors},
    } = methods;
    
    const onSubmit = async (data) => {
        try {
            const formattedPermissions = Object.keys(prefillPermission)
            .filter((key) => prefillPermission[key])
            .map((key) => `/${key}`);
            const response = await updateRole(id, data.role, data.description, formattedPermissions);
            if (response.error) {
                setError('afterSubmit', {
                  message: response.error,
                });
                return;
            }
            socket.emit("Edit!", {message: `${user?.name} updated the role ${data.role}!`});
            // enqueueSnackbar('Edit Role success!', "success");
            reset();
            resetPermission();
            onClose();
        } catch (error) {
            enqueueSnackbar('Something went wrong..', {variant:"error"});
            console.log(error);
        }
    };  

  return (
      <Dialog fullWidth maxWidth="xs" open={open} onClose={() => { onClose(); reset(); resetPermission(); }}>
        <DialogTitle sx={{ pb: 2 }}>{title}</DialogTitle>
        <IconButton
          aria-label="close"
          onClick={() => { onClose(); reset(); resetPermission(); }}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
            <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
                <DialogContent dividers>
                    <Stack spacing={3}>
                    {!!errors.afterSubmit && <Alert severity="error">{errors.afterSubmit.message}</Alert>}
                        <RHFTextField
                            name="role"
                            label="Role"
                            sx={{ mt: 1 }}
                        />
                        <RHFTextField
                            name="description"
                            label="Description"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={prefillPermission.employeemodule} onChange={handlePermissionChange} name="employeemodule" />
                            }
                            label="Employee Module"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={prefillPermission.usermanagement} onChange={handlePermissionChange} name="usermanagement" />
                            }
                            label="User Management"
                        />
                        <FormControlLabel
                            control={
                                <Switch checked={prefillPermission.rolemodule} onChange={handlePermissionChange} name="rolemodule" />
                            }
                            label="Role Module"
                        />
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button fullWidth variant="contained" color="success" type="submit">
                        Edit Role
                    </Button>
                </DialogActions>
            </FormProvider>
      </Dialog>
  );
}
