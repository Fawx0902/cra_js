import { createSlice } from '@reduxjs/toolkit';
// utils
import axios from '../../utils/axios';

// ----------------------------------------------------------------------

const initialState = {
    isLoading: false,
    error: null,
    users: [],
  };

const slice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        // START LOADING
        startLoading(state) {
            state.isLoading = true;
        },
        // HAS ERROR
        hasError(state, action) {
            state.isLoading = false;
            state.error = action.payload;
        },
        // GET USERS
        getUsers(state, action) {
            const { users } = action.payload;

            state.isLoading = false;
            state.users = users;

        },
    },
});

// Reducer
export default slice.reducer;
// Actions
/* export const {  } = slice.actions; */
// ----------------------------------------------------------------------

export function fetchUsers(payload) {
 
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {
        const result = await axios.get('/api/user/fetchusers', payload);
        dispatch(slice.actions.getUsers(result.data));

    } catch (error) {
        dispatch(slice.actions.hasError(error));
    }
  };
}