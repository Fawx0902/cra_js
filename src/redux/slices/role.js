import { createSlice } from '@reduxjs/toolkit';
// utils
import axios from '../../utils/axios';

// ----------------------------------------------------------------------

const initialState = {
    isLoading: false,
    error: null,
    roles: [],
  };

const slice = createSlice({
    name: 'role',
    initialState,
    reducers: {
        // START LOADING
        startLoading(state) {
            state.isLoading = true;
        },
        // HAS ERROR
        hasError(state, action) {
            state.isLoading = false;
            state.error = action.payload;
        },
        // GET ROLES
        getRoles(state, action) {
            const { roles } = action.payload;

            state.isLoading = false;
            state.roles = roles;
        },

    },
});

// Reducer
export default slice.reducer;
// Actions
/* export const {  } = slice.actions; */
// ----------------------------------------------------------------------

export function fetchRoles(payload) {
 
  return async (dispatch) => {
    dispatch(slice.actions.startLoading());
    try {
        const result = await axios.get('/api/role/all', payload);
        console.log(dispatch(slice.actions.getRoles(result.data)));
        dispatch(slice.actions.getRoles(result.data));

    } catch (error) {   
        dispatch(slice.actions.hasError(error));
    }
  };
}