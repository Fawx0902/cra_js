import PropTypes from 'prop-types';
import { useState } from 'react';
// @mui
import {
  Button,
  TableRow,
  MenuItem,
  TableCell,
  IconButton,
  Typography,
} from '@mui/material';
// Auth
import { useAuthContext } from '../../../../auth/useAuthContext';
// components
import Label from '../../../../components/label';
import Iconify from '../../../../components/iconify';
import MenuPopover from '../../../../components/menu-popover';
import { useSnackbar } from '../../../../components/snackbar';
import ConfirmDialog from '../../../../components/confirm-dialog';
import EditUserDialog from '../../../../components/edituser-dialog';
// API
import { deleteUser, restoreUser } from '../../../../pages/auth/API/user';

// ----------------------------------------------------------------------

UserRoleTableRow.propTypes = {
  row: PropTypes.object,
  selected: PropTypes.bool,
  onSelectRow: PropTypes.func,
  rolesNames: PropTypes.array,
};

export default function UserRoleTableRow({ row, selected, onSelectRow, rolesNames }) {

  const { id, name, email, role, created_at, deleted_at } = row;

  const { user } = useAuthContext();

  const [openConfirm, setOpenConfirm] = useState(false); // (Soft) Delete Dialog

  const [openEdit, setOpenEdit] = useState(false); // Edit Dialog

  const [openRestore, setOpenRestore] = useState(false); // Restore Soft-Delete Dialog

  const [openPopover, setOpenPopover] = useState(null);

  const { enqueueSnackbar } = useSnackbar();

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleOpenRestore = () => {
    setOpenRestore(true);
  };

  const handleCloseRestore = () => {
    setOpenRestore(false);
  }; 

  const handleOpenPopover = (event) => {
    setOpenPopover(event.currentTarget);
  };

  const handleClosePopover = () => {
    setOpenPopover(null);
  };

  const onSubmitDelete = async () => {
    try {
        if (email === user?.email) {
          enqueueSnackbar('You are still logged in that account..!', {variant:"warning"});
          handleCloseConfirm();
        } else {
          const response = deleteUser(id);
          if (response.error) {
            enqueueSnackbar(response.error, {variant: "error"});
            handleCloseConfirm();
          } else {
            enqueueSnackbar('Archive User successfully!', {variant:"success"});
            handleCloseConfirm();
          }
        }
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };

  const onSubmitRestore = async () => {
    try {
        const response = restoreUser(id);
        if (response.error) {
          enqueueSnackbar(response.error, {variant: "error"});
          handleCloseRestore();
        } else {
          enqueueSnackbar('User restored successfully!', {variant:"success"});
          handleCloseRestore();
        }
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };
  
  // Convert MySQL time to readable time
  function formatTimestamp(timestamp) {
    const date = new Date(timestamp);
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = date.toLocaleDateString(undefined, options);
    
    const timeOptions = { hour: 'numeric', minute: 'numeric' };
    const formattedTime = date.toLocaleTimeString(undefined, timeOptions);

    return `${formattedDate} ${formattedTime}`;
  }

  const timeStamp = formatTimestamp(created_at);

  // ICONS:
  // Activate/Deactivate: mdi:clipboard-check-outline | mdi:clipboard-remove-outline or eva:bookmark-outline
  // Archive: eva:archive-outline

  return (
    <>
      <TableRow hover selected={selected}>
        <TableCell align="center">
            <Typography variant="subtitle2" noWrap>
              {name}
            </Typography>
        </TableCell>

        <TableCell align="center">{email}</TableCell>

        <TableCell align="center" sx={{ textTransform: 'capitalize' }}>
              {role}
        </TableCell>

        <TableCell align="center" sx={{ textTransform: 'capitalize' }}>
              {timeStamp}
        </TableCell>

        <TableCell align="center">
          {deleted_at !== null ? (
            <Label
              variant="soft"
              color="error"
              sx={{ textTransform: 'capitalize' }}
            >
              Archive
            </Label>
          ) : (
            <Label
              variant="soft"
              color="success"
              sx={{ textTransform: 'capitalize' }}
            >
              Active
            </Label>
          )}
        </TableCell>

        <TableCell align="right">
          <IconButton color={openPopover ? 'inherit' : 'default'} onClick={handleOpenPopover}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <MenuPopover
        open={openPopover}
        onClose={handleClosePopover}
        arrow="right-top"
        sx={{ width: 140 }}
      >
      {deleted_at === null ? (
        <>
          <MenuItem
            onClick={() => {
              handleOpenEdit();
              handleClosePopover();
            }}
          >
            <Iconify icon="eva:edit-fill" />
            Edit
          </MenuItem>

          <MenuItem
            onClick={() => {
              handleOpenConfirm();
              handleClosePopover();
            }}
            sx={{ color: 'error.main' }}
          >
            <Iconify icon="eva:archive-outline" />
            Archive
          </MenuItem>
        </>
      ) : (
        <MenuItem
          onClick={() => {
            handleOpenRestore();
            handleClosePopover();
          }}
          sx={{ color: 'secondary.main' }}
        >
          <Iconify icon="eva:undo-fill" />
          Restore
        </MenuItem>
      )}
      </MenuPopover>

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Archive"
        content="Are you sure want to archive this user?"
        action={
          <Button variant="contained" color="error" onClick={() => { onSubmitDelete(); }}>
            Archive
          </Button>
        }
      />

      <ConfirmDialog
        open={openRestore}
        onClose={handleCloseRestore}
        title="Restore"
        content="Are you sure want to restore this user?"
        action={
          <Button variant="contained" color="warning" onClick={() => { onSubmitRestore(); }}>
            Restore
          </Button>
        }
      />

      <EditUserDialog
        open={openEdit}
        onClose={handleCloseEdit}
        title="Edit User"
        rolesNames = {rolesNames}
        row={row}
      />
    </>
  );
}
