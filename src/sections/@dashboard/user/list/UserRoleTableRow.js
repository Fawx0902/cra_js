import PropTypes from 'prop-types';
import { useState } from 'react';
// @mui
import {
  Button,
  TableRow,
  MenuItem,
  TableCell,
  IconButton,
  Typography,
} from '@mui/material';
// components
import Label from '../../../../components/label';
import Iconify from '../../../../components/iconify';
import MenuPopover from '../../../../components/menu-popover';
import { useSnackbar } from '../../../../components/snackbar';
import ConfirmDialog from '../../../../components/confirm-dialog';
import EditRoleDialog from '../../../../components/editrole-dialog';
// API
import { statusRole, deleteRole, restoreRole } from '../../../../pages/auth/API/role';

// ----------------------------------------------------------------------

UserRoleTableRow.propTypes = {
  row: PropTypes.object,
  selected: PropTypes.bool,
  onSelectRow: PropTypes.func,
  user: PropTypes.object,
  socket: PropTypes.object,
};

export default function UserRoleTableRow({ row, selected, onSelectRow, user, socket }) {
  const { id, role, description, isActive, created_at, deleted_at } = row;

  const [openConfirm, setOpenConfirm] = useState(false); // (Soft) Delete Dialog

  const [openEdit, setOpenEdit] = useState(false); // Edit Dialog

  const [openStatus, setOpenStatus] = useState(false); // Archive/Activate Dialog

  const [openRestore, setOpenRestore] = useState(false); // Restore Soft-Delete Dialog

  const [openPopover, setOpenPopover] = useState(null);

  const { enqueueSnackbar } = useSnackbar();

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };
  
  const handleOpenStatus = () => {
    setOpenStatus(true);
  };

  const handleCloseStatus = () => {
    setOpenStatus(false);
  }; 

  const handleOpenRestore = () => {
    setOpenRestore(true);
  };

  const handleCloseRestore = () => {
    setOpenRestore(false);
  }; 

  const handleOpenPopover = (event) => {
    setOpenPopover(event.currentTarget);
  };

  const handleClosePopover = () => {
    setOpenPopover(null);
  };

  const onSubmitStatus = async () => {
    try {
        await statusRole(id);
        enqueueSnackbar('Role status successfully updated!', {variant:"success"});
        handleCloseStatus();
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };

  const onSubmitDelete = async () => {
    try {
        await deleteRole(id);
        enqueueSnackbar('Archive Role successfully!', {variant:"success"});
        handleCloseConfirm();
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };

  const onSubmitRestore = async () => {
    try {
        await restoreRole(id);
        enqueueSnackbar('Role restored successfully!', {variant:"success"});
        handleCloseRestore();
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };
  
  // Convert MySQL time to readable time
  function formatTimestamp(timestamp) {
    const date = new Date(timestamp);
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const formattedDate = date.toLocaleDateString(undefined, options);
    
    const timeOptions = { hour: 'numeric', minute: 'numeric' };
    const formattedTime = date.toLocaleTimeString(undefined, timeOptions);

    return `${formattedDate} ${formattedTime}`;
  }

  const timeStamp = formatTimestamp(created_at);

  return (
    <>
      <TableRow hover selected={selected}>
        <TableCell align="center">
            <Typography variant="subtitle2" noWrap>
              {role}
            </Typography>
        </TableCell>

        <TableCell align="center">{description}</TableCell>

        <TableCell align="center" sx={{ textTransform: 'capitalize' }}>
              {timeStamp}
        </TableCell>

        <TableCell align="center">
        {deleted_at !== null ? (
          <Label
            variant="soft"
            color="error"
            sx={{ textTransform: 'capitalize' }}
          >
            Archive
          </Label>
        ) : (
          <Label
            variant="soft"
            color={isActive === 0 ? 'warning' : 'success'}
            sx={{ textTransform: 'capitalize' }}
          >
            {isActive === 0 ? 'Inactive' : 'Active'}
          </Label>
        )}
        </TableCell>

        <TableCell align="right">
          <IconButton color={openPopover ? 'inherit' : 'default'} onClick={handleOpenPopover}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <MenuPopover
        open={openPopover}
        onClose={handleClosePopover}
        arrow="right-top"
        sx={{ width: 140 }}
      >
      {deleted_at === null ? (
        <>
          <MenuItem
            onClick={() => {
              handleOpenEdit();
              handleClosePopover();
            }}
          >
            <Iconify icon="eva:edit-fill" />
            Edit
          </MenuItem>

          <MenuItem
            onClick={() => {
              handleOpenStatus();
              handleClosePopover();
            }}
            sx={{color: 'info.main'}}
          >
            <Iconify icon="eva:bookmark-outline" />
            {isActive === 0 ? 'Activate' : 'Deactivate'}
          </MenuItem>

          <MenuItem
            onClick={() => {
              handleOpenConfirm();
              handleClosePopover();
            }}
            sx={{ color: 'error.main' }}
          >
            <Iconify icon="eva:archive-outline" />
            Archive
          </MenuItem>
        </>
      ) : (
        <MenuItem
          onClick={() => {
            handleOpenRestore();
            handleClosePopover();
          }}
          sx={{ color: 'secondary.main' }}
        >
          <Iconify icon="eva:undo-fill" />
          Restore
        </MenuItem>
      )}
      </MenuPopover>

      <ConfirmDialog
        open={openStatus}
        onClose={handleCloseStatus}
        title="Update Role Status"
        content={isActive === 0 ? "Are you sure want to activate this role?" : "Are you sure want to deactivate this role?"}
        action={
          <Button variant="contained" color={isActive === 0 ? "success" : "warning"} onClick={() => { onSubmitStatus(); }}>
            {isActive === 0 ? 'Activate' : 'Deactivate'}
          </Button>
        }
      />

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Archive"
        content="Are you sure want to archive this role?"
        action={
          <Button variant="contained" color="error" onClick={() => { onSubmitDelete(); }}>
            Archive
          </Button>
        }
      />

      <ConfirmDialog
        open={openRestore}
        onClose={handleCloseRestore}
        title="Restore"
        content="Are you sure want to restore this role?"
        action={
          <Button variant="contained" color="warning" onClick={() => { onSubmitRestore(); }}>
            Restore
          </Button>
        }
      />

      <EditRoleDialog
        open={openEdit}
        onClose={handleCloseEdit}
        title="Edit Role"
        row={row}
        user={user}
        socket={socket}
      />
    </>
  );
}
