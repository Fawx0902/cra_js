import PropTypes from 'prop-types';
// @mui
import { Stack, InputAdornment, TextField, MenuItem, Button } from '@mui/material';
// components
import Iconify from '../../../../components/iconify';

// ----------------------------------------------------------------------

UserTestTableToolbar.propTypes = {
  isFiltered: PropTypes.bool,
  filterName: PropTypes.string,
  filterRole: PropTypes.string,
  onFilterName: PropTypes.func,
  onFilterRole: PropTypes.func,
  onResetFilter: PropTypes.func,
  rolesData: PropTypes.array,
};

export default function UserTestTableToolbar({
  isFiltered,
  filterName,
  filterRole,
  onFilterName,
  onFilterRole,
  onResetFilter,
  rolesData,
}) 
{

    return (
        <Stack
        spacing={2}
        alignItems="center"
        direction={{
            xs: 'column',
            sm: 'row',
        }}
        sx={{ px: 2.5, py: 3 }}
        >
        <TextField
        fullWidth
        select
        label="Role"
        value={filterRole}
        onChange={onFilterRole}
        SelectProps={{
          MenuProps: {
            PaperProps: {
              sx: {
                maxHeight: 260,
              },
            },
          },
        }}
        sx={{
          maxWidth: { sm: 240 },
          textTransform: 'capitalize',
        }}
      >
        <MenuItem
            key='all'
            value='all'
            sx={{
              mx: 1,
              borderRadius: 0.75,
              typography: 'body2',
              textTransform: 'capitalize',
            }}
          >
            all
        </MenuItem>
        {rolesData
        ? (rolesData
        .filter((option) => option.deleted_at === null)
        .map((option) => (
            <MenuItem
              key={option.role}
              value={option.role}
              sx={{
                mx: 1,
                borderRadius: 0.75,
                typography: 'body2',
                textTransform: 'capitalize',
              }}
            >
              {option.role}
            </MenuItem>
          ))) : (
            <MenuItem key="loading" disabled>
              Loading...
            </MenuItem>
          )}
      </TextField>
        <TextField
            fullWidth
            value={filterName}
            onChange={onFilterName}
            placeholder="Search..."
            InputProps={{
            startAdornment: (
                <InputAdornment position="start">
                <Iconify icon="eva:search-fill" sx={{ color: 'text.disabled' }} />
                </InputAdornment>
            ),
            }}
        />

        {isFiltered && (
            <Button
            color="error"
            sx={{ flexShrink: 0 }}
            onClick={onResetFilter}
            startIcon={<Iconify icon="eva:trash-2-outline" />}
            >
            Clear
            </Button>
        )}
        </Stack>
    );
    }
