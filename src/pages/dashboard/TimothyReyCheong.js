import { Helmet } from 'react-helmet-async';
// @mui
import { Container, Typography, Button, Modal, Box } from '@mui/material';
// components
import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useSettingsContext } from '../../components/settings';

// ----------------------------------------------------------------------
export default function BlankPage() {
  const { themeStretch } = useSettingsContext();
  const { pathname } = useLocation();
  const [open, setOpen] = useState(false);
  const sampleModalOpen = () => setOpen(true);
  const sampleModalClose = () => setOpen(false);

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  const lastPathnamePart = pathname.substring(pathname.lastIndexOf('/') + 1);

  return (
    <>
      <Helmet>
        <title> Sample Page | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Button onClick={sampleModalOpen}>Click Me!</Button>
        <Modal
          open={open}
          onClose={sampleModalClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        > 
          <Box sx={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Hello there!
            </Typography>
            <Typography id="modal-modal-description" sx={{ mt: 2 }}>
              You are currently in the {lastPathnamePart} page!
            </Typography>
          </Box>
        </Modal>
      </Container>
    </>
  );
}
