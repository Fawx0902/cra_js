import { Helmet } from 'react-helmet-async';
import { useState, useEffect, useCallback } from 'react';
// @mui
import { 
  Button,  
  Card, 
  Container, 
  Divider,
  Stack,  
  Tabs, 
  Tab, 
  Table, 
  TableBody, 
  TableContainer,
  Typography,  } from '@mui/material';
import { useSnackbar } from '../../components/snackbar';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// components
import Iconify from '../../components/iconify';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import CreateUserDialog from '../../components/createuser-dialog';
import ConfirmDialog from '../../components/confirm-dialog';
import { useSettingsContext } from '../../components/settings';
import Scrollbar from '../../components/scrollbar';
import {
  useTable,
  getComparator,
  emptyRows,
  TableNoData,
  TableEmptyRows,
  TableHeadRoleCustom,
  TablePaginationCustom,
} from '../../components/table';
import { Upload } from '../../components/upload';
// sections
import { UserTestTableToolbar, UserTestTableRow } from '../../sections/@dashboard/user/list';

// APIs
import { useDispatch, useSelector } from '../../redux/store';
import { fetchRoles } from '../../redux/slices/role';
import { fetchUsers } from '../../redux/slices/user';
import { exportUser, importUser } from '../auth/API/user';

// ----------------------------------------------------------------------
export default function UserTestListPage() {

  // VARIABLES
  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    setPage,
    //
    selected,
    onSelectRow,
    //
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = useTable();

  // Display All or Archive (Soft-Deleted) accounts
  const STATUS_OPTIONS = ['all', 'archived'];

  // Display the header of the tables
  const TABLE_HEAD = [
    { id: 'name', label: 'Name', align: 'center' },
    { id: 'email', label: 'Email', align: 'center' },
    { id: 'role', label: 'Role', align: 'center' },
    { id: 'created_at', label: 'Created At', align: 'center' },
    { id: 'status', label: 'Status', align: 'center' },
    { id: '' },
  ];

  const { enqueueSnackbar } = useSnackbar();
  const { roles } = useSelector((state) => state.role);
  const { users } = useSelector((state) => state.user);
  const [filterName, setFilterName] = useState('');
  const [filterRole, setFilterRole] = useState('all');
  const [filterStatus, setFilterStatus] = useState('all');
  const [openCreate, setOpenCreate] = useState(false);
  const [openImport, setOpenImport] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [rolesData, setRolesData] = useState([]);
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRoles());
    dispatch(fetchUsers());
  }, [dispatch]);

  useEffect(() => {
    setTableData(users);
    setRolesData(roles);
  }, [users, roles]);

  const dataFiltered = applyFilter({
    inputData: tableData,
    comparator: getComparator(order, orderBy),
    filterName,
    filterRole,
    filterStatus,
  });

  const denseHeight = dense ? 52 : 72;
  
  const isFiltered = filterName !== '' || filterRole !== 'all' || filterStatus !== 'all';
  const isNotFound =
    (!dataFiltered.length && !!filterName) ||
    (!dataFiltered.length && !!filterRole) ||
    (!dataFiltered.length && !!filterStatus);

  const { themeStretch } = useSettingsContext();

  // FUNCTIONS

  const handleFilterStatus = (event, newValue) => {
    setPage(0);
    setFilterStatus(newValue);
  };

  const handleFilterName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const handleFilterRole = (event) => {
    setPage(0);
    setFilterRole(event.target.value);
  };

  const handleResetFilter = () => {
    setFilterName('');
    setFilterRole('all');
    setFilterStatus('all');
  };

  const handleOpenCreate = () => {
    setOpenCreate(true);
  }

  const handleCloseCreate = () => {
    setOpenCreate(false);
  }

  const handleOpenImport = () => {
    setOpenImport(true);
  }

  const handleCloseImport = () => {
    setOpenImport(false);
    setFile(null);
  }

  const handleDropSingleFile = useCallback((acceptedFiles) => {
    const newFile = acceptedFiles[0];
    if (newFile) {
      setFile(
        Object.assign(newFile, {
          preview: URL.createObjectURL(newFile),
        })
      );
    }
  }, []);

  const submitImport = async () => {
    const response = await importUser(file);
    if (response.error) {
      enqueueSnackbar(response.error, {variant: 'error'});
      return;
    }
    enqueueSnackbar('Users successfully imported!', {variant:"success"});
    handleCloseImport();
  }

  // Debugging Purpose if it prints
  // console.log(roles);
  // console.log(users);
  // console.log(rolesData);
  // console.log(file);

  return (
    <>
      <Helmet>
        <title> User List: CRUD | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
          heading="User List: CRUD"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'User', href: PATH_DASHBOARD.user.root },
            { name: 'List' },
          ]}
          action={
            <>
              <Button
                variant="contained"
                startIcon={<Iconify icon="eva:cloud-download-fill" />}
                sx={{ mx: 1 }}
                onClick={() => {
                  exportUser();
                }}
              >
                Export
              </Button>

              <Button
                variant="contained"
                startIcon={<Iconify icon="eva:cloud-upload-fill" />}
                sx={{ mx: 1 }}
                onClick={() => {
                  handleOpenImport();
                }}
              >
                Import
              </Button>

              <Button
                variant="contained"
                startIcon={<Iconify icon="eva:plus-fill" />}
                sx={{ mx: 1 }}
                onClick={() => {
                  handleOpenCreate();
                }}
              >
                New User
              </Button>
            </>
          }
        />

        <Card>
          <Tabs
            value={filterStatus}
            onChange={handleFilterStatus}
            sx={{
              px: 2,
              bgcolor: 'background.neutral',
            }}
          >
            {STATUS_OPTIONS.map((tab) => {
              let tabValue
              if (tab === 'archive') {
                tabValue = 1
              } else {
                tabValue = tab
              }
              return <Tab key={tab} label={tab} value={tabValue} />
            })}
          </Tabs>

          <Divider />

          <UserTestTableToolbar
            isFiltered={isFiltered}
            filterName={filterName}
            filterRole={filterRole}
            rolesData={rolesData}
            onFilterName={handleFilterName}
            onFilterRole={handleFilterRole}
            onResetFilter={handleResetFilter}
          />
          
          <TableContainer sx={{ position: 'relative', overflow: 'unset' }}>
            <Scrollbar>
              <Table size={dense ? 'small' : 'medium'} sx={{ minWidth: 800 }}>
                <TableHeadRoleCustom
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={tableData.length}
                  onSort={onSort}
                />

                <TableBody>
                  {dataFiltered
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => (
                      <UserTestTableRow
                        key={row.id}
                        row={row}
                        selected={selected.includes(row.id)}
                        onSelectRow={() => onSelectRow(row.id)}
                        rolesNames = {rolesData}
                      />
                    ))}

                  <TableEmptyRows
                    height={denseHeight}
                    emptyRows={emptyRows(page, rowsPerPage, tableData.length)}
                  />

                  <TableNoData isNotFound={isNotFound} />
                </TableBody>
              </Table>
            </Scrollbar>
          </TableContainer>

          <TablePaginationCustom
            count={dataFiltered.length}
            page={page}
            rowsPerPage={rowsPerPage}
            onPageChange={onChangePage}
            onRowsPerPageChange={onChangeRowsPerPage}
            dense={dense}
            onChangeDense={onChangeDense}
          />
        </Card>
      </Container>

      <CreateUserDialog 
        open={openCreate}
        onClose={handleCloseCreate}
        title="New User"
        rolesNames = {rolesData}
      />

      <ConfirmDialog
        open={openImport}
        onClose={handleCloseImport}
        title="Import User"
        content={
          <Stack spacing={3}>
            <Upload 
            file={file}
            onDrop={handleDropSingleFile}
            onDelete={() => setFile(null)}
            />
            <Typography
                variant="caption"
                sx={{
                  mt: 2,
                  mx: 'auto',
                  display: 'block',
                  textAlign: 'center',
                  color: 'text.secondary',
                }}
              >
                Allowed *xlsx and *csv only.
            </Typography>
          </Stack>
        }
        action={
          <Button
            variant="contained"
            color="success"
            onClick={() => {
              submitImport();
              handleCloseImport();
            }}
          >
            Import
          </Button>
        }
      />
    </>
  );
}


// ------------------------------------------------------ //

function applyFilter({ inputData, comparator, filterName, filterStatus, filterRole }) {
    const stabilizedThis = inputData.map((el, index) => [el, index]);
  
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
  
    inputData = stabilizedThis.map((el) => el[0]);
  
    if (filterName) {
      inputData = inputData.filter(
        (user) => user.name.toLowerCase().indexOf(filterName.toLowerCase()) !== -1
      );
    }
  
    else if (filterStatus !== 'all') {
      inputData = inputData.filter((user) => user.deleted_at !== null);
    } else {
      inputData = inputData.filter((user) => user.deleted_at === null);
    }
  
    if (filterRole !== 'all') {
      inputData = inputData.filter((user) => user.role === filterRole);
    }
  
    return inputData;
  }
