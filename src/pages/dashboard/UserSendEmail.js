import { Helmet } from 'react-helmet-async';
import * as Yup from 'yup';
// @mui
import { Alert, Container, Typography, Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// routes
import { PATH_DASHBOARD } from '../../routes/paths';
// components
import { useSettingsContext } from '../../components/settings';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import FormProvider, { RHFTextField } from '../../components/hook-form';
import { useSnackbar } from '../../components/snackbar';
// API
import { sendEmail } from '../auth/API/email';
// ----------------------------------------------------------------------
export default function UserSendEmail() {

  const { themeStretch } = useSettingsContext();
  const { enqueueSnackbar } = useSnackbar();

  const MailSchema = Yup.object().shape({
    email: Yup.string().required('Email is required!').email('Email must be a valid email address..'),
    subject: Yup.string().required('Subject is required!'),
    content: Yup.string().required('Content is required!'),
  });

  const defaultValues = {
    email: '',
    subject: '',
    content: '',
  }

  const methods = useForm({
    resolver: yupResolver(MailSchema),
    defaultValues,
  });

  const {
    reset,
    setError,
    handleSubmit,
    formState: {errors, isSubmitting, isSubmitSuccessful},
  } = methods;

  const onSubmit = async (data) => {
    try {
        const response = await sendEmail(data.email, data.subject, data.content);
        if (response.error) {
            setError('afterSubmit', {
              message: response.error,
            });
            return;
        }
        enqueueSnackbar('Email sent successfully!' , {variant:"success"});
        reset();
    } catch (error) {
        enqueueSnackbar('Something went wrong..', {variant:"error"});
        console.log(error);
    }
  };

  console.log(isSubmitting);
  console.log(isSubmitSuccessful);

  return (
    <>
      <Helmet>
        <title> Email | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
            heading="User Email"
            links={[
                { name: 'Dashboard', href: PATH_DASHBOARD.root },
                { name: 'User', href: PATH_DASHBOARD.user.root },
                { name: 'Email' },
            ]}
            />
      </Container>

      <Container maxWidth={themeStretch ? false : 'md'}>
        <Stack
            sx={{
              borderRadius: 2,
            }}
        >
            <Typography variant="h3"
              sx={{
                py: 3,
                px: 3,
              }}> Contact us! </Typography>

            <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
                {!!errors.afterSubmit && <Alert severity="error">{errors.afterSubmit.message}</Alert>}
                <Stack spacing={3} sx={{ p: 3 }}>
                    <RHFTextField
                        name="email"
                        label="Email"
                        sx={{ mt: 1 }}
                    />

                    <RHFTextField
                        name="subject"
                        label="Subject"
                        sx={{ mt: 1 }}
                    />

                    <RHFTextField
                        multiline
                        rows={4}
                        name="content"
                        label="Content"
                        sx={{ mt: 1 }}
                    />

                    <LoadingButton
                    fullWidth
                    color="success"
                    type="submit"
                    variant="contained"
                    loading={isSubmitting}
                    sx={{
                      p:1,
                    }}
                    >
                    Send Email
                    </LoadingButton>
                </Stack>
            </FormProvider>
        
        </Stack>
      </Container>
    </>
  );
}
