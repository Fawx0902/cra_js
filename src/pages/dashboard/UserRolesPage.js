import { Helmet } from 'react-helmet-async';
import { useState, useEffect } from 'react';
// @mui
import { 
  Button,  
  Card, 
  Container, 
  Divider,  
  Tabs, 
  Tab, 
  Table, 
  TableBody, 
  TableContainer, } from '@mui/material';
// routes
import io from "socket.io-client";
import { PATH_DASHBOARD } from '../../routes/paths';
// components
import Iconify from '../../components/iconify';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import CreateRoleDialog from '../../components/createrole-dialog';
import { useSettingsContext } from '../../components/settings';
import Scrollbar from '../../components/scrollbar';
import {
  useTable,
  getComparator,
  emptyRows,
  TableNoData,
  TableEmptyRows,
  TableHeadRoleCustom,
  TablePaginationCustom,
} from '../../components/table';
import { useSnackbar } from '../../components/snackbar';
// sections
import { UserRoleToolbar, UserRoleTableRow } from '../../sections/@dashboard/user/list';

// redux
import { useDispatch, useSelector } from '../../redux/store';
import { fetchRoles } from '../../redux/slices/role';
import { useAuthContext } from '../../auth/useAuthContext';
import audio from '../../assets/alert.mp3';

// ----------------------------------------------------------------------
export default function UserRolesPagae() {

  // VARIABLES
  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    setPage,
    //
    selected,
    onSelectRow,
    //
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = useTable();

  // Display All, Active, Inactive or Archive (Soft-Deleted) roles
  const STATUS_OPTIONS = ['all', 'active', 'inactive', 'archive'];

  // Display the header of the tables
  const TABLE_HEAD = [
    { id: 'role', label: 'Role', align: 'center' },
    { id: 'description', label: 'Description', align: 'center' },
    { id: 'created_at', label: 'Created at', align: 'center' },
    { id: 'isActive', label: 'Status', align: 'center' },
    { id: '' },
  ];
  const { user } = useAuthContext();
  const socket = io.connect(process.env.REACT_APP_SOCKET_KEY);
  const { roles } = useSelector((state) => state.role);
  const [filterName, setFilterName] = useState('');
  const [filterRole, setFilterRole] = useState('all');
  const [filterStatus, setFilterStatus] = useState('all');
  const [openCreate, setOpenCreate] = useState(false);
  const [tableData, setTableData] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRoles());
  }, [dispatch]);

  useEffect(() => {
    setTableData(roles);
  }, [roles]);

  console.log(fetchRoles());

  const playAudio = () => {
    new Audio(audio).play();
  }
  
  useEffect(() => {
    socket.on("Receive Add!", (data) => {
      enqueueSnackbar(data.message, {variant: 'info'});
      playAudio();
      dispatch(fetchRoles());
    });

    socket.on("Receive Edit!", (data) => {
      enqueueSnackbar(data.message, {variant: 'info'});
      playAudio();
      dispatch(fetchRoles());
    });

    return () => {
      socket.off("Receive Add!");
      socket.off("Receive Edit!");
    }
  }, [socket, enqueueSnackbar, dispatch]);

  const dataFiltered = applyFilter({
    inputData: tableData,
    comparator: getComparator(order, orderBy),
    filterName,
    filterRole,
    filterStatus,
  });

  const denseHeight = dense ? 52 : 72;
  
  const isFiltered = filterName !== '' || filterRole !== 'all' || filterStatus !== 'all';
  const isNotFound =
    (!dataFiltered.length && !!filterName) ||
    (!dataFiltered.length && !!filterRole) ||
    (!dataFiltered.length && !!filterStatus);

  const { themeStretch } = useSettingsContext();

  // FUNCTIONS

  const handleFilterStatus = (event, newValue) => {
    setPage(0);
    setFilterStatus(newValue);
  };

  const handleFilterName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const handleFilterRole = (event) => {
    setPage(0);
    setFilterRole(event.target.value);
  };

  const handleResetFilter = () => {
    setFilterName('');
    setFilterRole('all');
    setFilterStatus('all');
  };

  const handleOpenCreate = () => {
    setOpenCreate(true);
  }

  const handleCloseCreate = () => {
    setOpenCreate(false);
  }

  // Debugging Purpose if it prints
  // console.log(roles);

  return (
    <>
      <Helmet>
        <title> User: Roles | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
          heading="User Roles"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'User', href: PATH_DASHBOARD.user.root },
            { name: 'Roles' },
          ]}
          action={
            <Button
              variant="contained"
              startIcon={<Iconify icon="eva:plus-fill" />}
              onClick={() => {
                handleOpenCreate();
              }}
            >
              New Role
            </Button>
          }
        />

        <Card>
          <Tabs
            value={filterStatus}
            onChange={handleFilterStatus}
            sx={{
              px: 2,
              bgcolor: 'background.neutral',
            }}
          >
            {STATUS_OPTIONS.map((tab) => {
              let tabValue
              if (tab === 'archive') {
                tabValue = 2
              } else if (tab === 'active') {
                tabValue = 1
              } else if (tab === 'inactive') {
                tabValue = 0
              } else {
                tabValue = tab
              }
              return <Tab key={tab} label={tab} value={tabValue} />
            })}
          </Tabs>

          <Divider />

          <UserRoleToolbar
            isFiltered={isFiltered}
            filterName={filterName}
            filterRole={filterRole}
            onFilterName={handleFilterName}
            onFilterRole={handleFilterRole}
            onResetFilter={handleResetFilter}
          />
          
          <TableContainer sx={{ position: 'relative', overflow: 'unset' }}>
            <Scrollbar>
              <Table size={dense ? 'small' : 'medium'} sx={{ minWidth: 800 }}>
                <TableHeadRoleCustom
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={tableData.length}
                  onSort={onSort}
                />

                <TableBody>
                  {dataFiltered
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => (
                      <UserRoleTableRow
                        key={row.id}
                        row={row}
                        selected={selected.includes(row.id)}
                        onSelectRow={() => onSelectRow(row.id)}
                        user={user}
                        socket={socket}
                      />
                    ))}

                  <TableEmptyRows
                    height={denseHeight}
                    emptyRows={emptyRows(page, rowsPerPage, tableData.length)}
                  />

                  <TableNoData isNotFound={isNotFound} />
                </TableBody>
              </Table>
            </Scrollbar>
          </TableContainer>

          <TablePaginationCustom
            count={dataFiltered.length}
            page={page}
            rowsPerPage={rowsPerPage}
            onPageChange={onChangePage}
            onRowsPerPageChange={onChangeRowsPerPage}
            //
            dense={dense}
            onChangeDense={onChangeDense}
          />
        </Card>
      </Container>

      <CreateRoleDialog
      open={openCreate}
      onClose={handleCloseCreate}
      title="New Role"
      user={user}
      socket={socket}
      />
    </>
  );
}


// ------------------------------------------------------ //

function applyFilter({ inputData, comparator, filterName, filterStatus, filterRole }) {
  const stabilizedThis = inputData.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });

  inputData = stabilizedThis.map((el) => el[0]);

  if (filterName) {
    inputData = inputData.filter(
      (role) => role.role.toLowerCase().indexOf(filterName.toLowerCase()) !== -1
    );
  }

  // Based on tabValue [2 - Archive, 1- Active, 0- Inactive]
  if (filterStatus === 2) {
    inputData = inputData.filter((role) => role.deleted_at !== null);
  }
  else if (filterStatus !== 'all') {
    inputData = inputData.filter((role) => role.isActive === filterStatus && role.deleted_at === null);
  } else {
    inputData = inputData.filter((role) => role.deleted_at === null);
  }

  if (filterRole !== 'all') {
    inputData = inputData.filter((role) => role.isActive === filterRole);
  }

  return inputData;
}
