import axios from '../../../utils/axios';

export async function createUser(name, email, password, role) {
    try {
        const response = await axios.post('/api/user/register', {
            name,
            email,
            password,
            role,
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function updateUser(id, name, email, role) {
    try {
        const response = await axios.put(`/api/user/update/${id}`, {
            name,
            email,
            role,
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function deleteUser(id){
    try {
        const response = await axios.delete(`/api/user/delete/${id}`);

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function restoreUser(id) {
    try {
        const response = await axios.patch(`/api/user/restore/${id}`);

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function importUser(file) {
    try {
        const formData = new FormData();
        formData.append('file', file);
        const response = await axios.post('/api/user/import', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function exportUser() {
    try {
        const response = await axios.get('/api/user/export', {
            responseType: 'blob',
        });

        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'userRecords.xlsx');
        document.body.appendChild(link);
        link.click();

        window.URL.revokeObjectURL(url);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}