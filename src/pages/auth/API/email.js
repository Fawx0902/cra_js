import axios from '../../../utils/axios';

export async function sendEmail(email, subject, content) {

    try {
        const response = await axios.post('/api/email/send', {
            email,
            subject,
            content,
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}