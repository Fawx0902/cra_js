import axios from '../../../utils/axios';

export async function createRole(role, description, permission, isActive) {

    try {
        const response = await axios.post('/api/role/create', {
            role,
            description,
            permission,
            isActive,
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function updateRole(id, role, description, permission) {

    try {
        const response = await axios.put(`/api/role/update/${id}`, {
            role,
            description,
            permission,
        });

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function statusRole(id) {
    try {
        const response = await axios.patch(`/api/role/status/${id}`);

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

// Soft Delete
export async function deleteRole(id) {
    try {
        const response = await axios.delete(`/api/role/delete/${id}`);

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}

export async function restoreRole(id) {
    try {
        const response = await axios.patch(`/api/role/restore/${id}`);

        console.log(response.data);
        return response.data;
    } catch (error) {
        console.log(error);
        return { data: null, error: error.message};
    }
}